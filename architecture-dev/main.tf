

locals {
  name_prefix = "us-west-2-dev"
  s3_region   = "us-east-1"
}

data "aws_region" "current" {}

data "aws_caller_identity" "current" {}

module "media-transcoder-v2" {
  source = "gitlab.com/victorbecerra/module-video-transcoder/local"
  #source = "./video-transcoder-v2"
  version = "~> 0.0"

  name_prefix     = local.name_prefix
  s3_input_bucket = "lmsfiles-dev"
  s3_region       = local.s3_region
}

output "lambda_arn" {
    value = module.media-transcoder-v2.lambda_function_arn
}

output "EventMediaConvert_enpoint" {
    value = module.media-transcoder-v2.EventMediaConvert_endpoint
}

output "EventMediaConvert_queue" {
    value = module.media-transcoder-v2.EventMediaConvert_queue
}

output "IAM_role_name" {
    value = module.media-transcoder-v2.iam_role_name
}

output "IAM_role_arn" {
    value = module.media-transcoder-v2.iam_role_arn
}

output "aws_caller_identity" {
  value = data.aws_caller_identity.current
}
