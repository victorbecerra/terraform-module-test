terraform {
  backend "http" {}
}

module "my_module_name" {
  source = "gitlab.com/victorbecerra/gitlab-file/local"
  version = "0.0.4"
  text = "Hello World"
  filename = "hello"
}

output "filesize_in_bytes" {
  value = module.my_module_name.bytes
}
